from glob import glob

def getScore(flist):
    dd = {}
    for i in flist:
        f = open(i,'r')
        lines = f.readlines()
        best = -1; keep = -1
        name = None
        for i in lines:
            if 'classify-train' in i:
                name = i
            if 'evaluation' in i:
                i = i.split()
                d = float(i[1])
                t = float(i[2])
                if d > best:
                    best = d
                    keep = t
            dd[name]=(best,keep)
    print len(dd)
    print len(flist)
    dd_sorted=sorted(dd.items(), key=lambda x: x[1][0], reverse=True)
    for i in dd_sorted:
        print i


flist = glob('slurm*out')
getScore(flist)