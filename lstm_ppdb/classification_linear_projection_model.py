
import theano
import numpy as np
from theano import tensor as T
from theano import config
import pdb
from evaluate import evaluate
from AverageLayer import AverageLayer
import time
import lasagne
import sys
import cPickle

def checkIfQuarter(idx,n):
    #print idx, n
    if idx==round(n/4.) or idx==round(n/2.) or idx==round(3*n/4.):
        return True
    return False

class classification_linear_projection_model(object):

    #takes list of seqs, puts them in a matrix
    #returns matrix of seqs and mask
    def prepare_data(self, list_of_seqs):
        lengths = [len(s) for s in list_of_seqs]
        n_samples = len(list_of_seqs)
        maxlen = np.max(lengths)
        x = np.zeros((n_samples, maxlen)).astype('int32')
        x_mask = np.zeros((n_samples, maxlen)).astype(theano.config.floatX)
        for idx, s in enumerate(list_of_seqs):
            x[idx, :lengths[idx]] = s
            x_mask[idx, :lengths[idx]] = 1.
        x_mask = np.asarray(x_mask,dtype=config.floatX)
        return x, x_mask

    def saveParams(self, fname):
        f = file(fname, 'wb')
        cPickle.dump(self.all_params, f, protocol=cPickle.HIGHEST_PROTOCOL)
        f.close()

    def get_minibatches_idx(self, n, minibatch_size, shuffle=False):
        idx_list = np.arange(n, dtype="int32")

        if shuffle:
            np.random.shuffle(idx_list)

        minibatches = []
        minibatch_start = 0
        for i in range(n // minibatch_size):
            minibatches.append(idx_list[minibatch_start:
                                    minibatch_start + minibatch_size])
            minibatch_start += minibatch_size

        if (minibatch_start != n):
            # Make a minibatch out of what is left
            minibatches.append(idx_list[minibatch_start:])

        return zip(range(len(minibatches)), minibatches)

    def getData(self, batch):
        #batch is list of tuples
        g1 = []; g2 = []
        for i in batch:
            g1.append(i[0].embeddings)
            g2.append(i[1].embeddings)

        g1x, g1mask = self.prepare_data(g1)
        g2x, g2mask = self.prepare_data(g2)

        scores = []
        for i in batch:
            temp = np.zeros(self.nout)
            score = float(i[2])
            ceil, fl = int(np.ceil(score)), int(np.floor(score))
            if ceil == fl:
                temp[fl-1] = 1
            else:
                temp[fl-1] = ceil - score
                temp[ceil-1] = score - fl
            #print temp, score
            scores.append(temp)
        scores = np.matrix(scores)+0.000001
        scores = np.asarray(scores,dtype=config.floatX)
        #print scores
        return (scores,g1x,g1mask,g2x,g2mask)

    def __init__(self, We_initial, params):

        #params
        initial_We = theano.shared(np.asarray(We_initial, dtype = config.floatX))
        We = theano.shared(np.asarray(We_initial, dtype = config.floatX))

        if regfile and not userep:
            initial_We = theano.shared(np.asarray(p[0].get_value(), dtype = config.floatX))
            We = theano.shared(np.asarray(p[0].get_value(), dtype = config.floatX))
            updatewords = True
        elif userep:
            We = theano.shared(np.asarray(p[0].get_value(), dtype = config.floatX))
            updatewords = False

        #symbolic params
        g1batchindices = T.imatrix(); g2batchindices = T.imatrix()
        g1mask = T.matrix(); g2mask = T.matrix()
        scores = T.matrix()

        if regfile or userep:
            W = np.asarray(p[1].get_value(), dtype = config.floatX)
            b = np.asarray(p[2].get_value(), dtype = config.floatX)


        #get embeddings
        l_in = lasagne.layers.InputLayer((None, None))
        l_mask = lasagne.layers.InputLayer(shape=(None, None))
        l_emb = lasagne.layers.EmbeddingLayer(l_in, input_size=We.get_value().shape[0], output_size=We.get_value().shape[1], W=We)
        l_average = AverageLayer([l_emb, l_mask])
        l_out = lasagne.layers.DenseLayer(l_average, params.layersize, nonlinearity=params.nonlinearity)

        if userep or regfile:
            l_in = lasagne.layers.InputLayer((None, None))
            l_mask = lasagne.layers.InputLayer(shape=(None, None))
            l_emb = lasagne.layers.EmbeddingLayer(l_in, input_size=We.get_value().shape[0], output_size=We.get_value().shape[1], W=We)
            l_average = AverageLayer([l_emb, l_mask])
            l_out = lasagne.layers.DenseLayer(l_average, params.layersize, nonlinearity=params.nonlinearity,W=W, b=b)


        embg1 = lasagne.layers.get_output(l_out, {l_in:g1batchindices, l_mask:g1mask})
        embg2 = lasagne.layers.get_output(l_out, {l_in:g2batchindices, l_mask:g2mask})

        #objective function
        g1_dot_g2 = embg1*embg2
        g1_abs_g2 = abs(embg1-embg2)

        lin_dot = lasagne.layers.InputLayer((None, params.layersize))
        lin_abs = lasagne.layers.InputLayer((None, params.layersize))
        l_sum = lasagne.layers.ConcatLayer([lin_dot, lin_abs])
        l_sigmoid = lasagne.layers.DenseLayer(l_sum, params.memsize, nonlinearity=lasagne.nonlinearities.sigmoid)
        l_softmax = lasagne.layers.DenseLayer(l_sigmoid, maxval-minval+1, nonlinearity=T.nnet.softmax)
        X = lasagne.layers.get_output(l_softmax, {lin_dot:g1_dot_g2, lin_abs:g1_abs_g2})
        Y = T.log(X)

        cost = scores*(T.log(scores) - Y)
        cost = cost.sum(axis=1)/(float(self.nout))

        prediction = 0.
        i = minval
        while i<= maxval:
            prediction = prediction + i*X[:,i-1]
            i += 1

        self.network_params = lasagne.layers.get_all_params(l_out, trainable=True) + lasagne.layers.get_all_params(l_softmax, trainable=True)
        self.network_params.pop(0)
        self.all_params = lasagne.layers.get_all_params(l_out, trainable=True) + lasagne.layers.get_all_params(l_softmax, trainable=True)
        print self.network_params

        #regularization
        l2 = 0.5*self.LC*sum(lasagne.regularization.l2(x) for x in self.network_params)
        if params.LRC:
            proj_params = lasagne.layers.get_all_params(l_out, trainable=True)
            l2 = 0.5*params.LRC*sum(lasagne.regularization.l2(x) for x in proj_params)
            network_params = lasagne.layers.get_all_params(l_softmax, trainable=True)
            l2 += 0.5*params.LC*sum(lasagne.regularization.l2(x) for x in network_params)
        if userep:
            self.network_params = lasagne.layers.get_all_params(l_softmax, trainable=True)
            l2 = 0.5*params.LC*sum(lasagne.regularization.l2(x) for x in self.network_params)
            cost = T.mean(cost) + l2
            self.all_params = self.network_params
        elif regfile:
            proj_params = lasagne.layers.get_all_params(l_out, trainable=True)
            l2 = 0.5*params.LRC*(lasagne.regularization.l2(proj_params[1]-np.asarray(p[1].get_value(), dtype = config.floatX)))
            l2 += 0.5*params.LRC*(lasagne.regularization.l2(proj_params[2]-np.asarray(p[2].get_value(), dtype = config.floatX)))
            network_params = lasagne.layers.get_all_params(l_softmax, trainable=True)
            l2 += 0.5*params.LC*sum(lasagne.regularization.l2(x) for x in network_params)
            #pdb.set_trace()
            if params.LRW:
                word_reg = 0.5*params.LRW*lasagne.regularization.l2(We-initial_We)
                cost = T.mean(cost) + l2 + word_reg
            else:
                print "Warning, LRW not set!"
                cost = T.mean(cost) + l2
        elif updatewords:
            word_reg = 0.5*params.LW*lasagne.regularization.l2(We-initial_We)
            cost = T.mean(cost) + l2 + word_reg
        else:
            cost = T.mean(cost) + l2

        self.feedforward_function = theano.function([g1batchindices,g1mask], embg1)
        self.scoring_function = theano.function([g1batchindices, g2batchindices,
                             g1mask, g2mask],prediction)
        self.Y_function = theano.function([g1batchindices, g2batchindices,
                             g1mask, g2mask],X)
        self.cost_function = theano.function([scores, g1batchindices, g2batchindices,
                             g1mask, g2mask], cost)

        #updates
        self.train_function = None
        if updatewords:
            updates = lasagne.updates.adagrad(cost, self.all_params, params.eta)
            self.train_function = theano.function([scores, g1batchindices, g2batchindices,
                             g1mask, g2mask], cost, updates=updates)
        else:
            self.all_params = self.network_params
            updates = lasagne.updates.adagrad(cost, self.all_params, params.eta)
            self.train_function = theano.function([scores, g1batchindices, g2batchindices,
                             g1mask, g2mask], cost, updates=updates)


    #trains parameters
    def train(self,train_data, dev, test, train, words, params):
        start_time = time.time()
        #evaluate_all(self,words)

        try:
            for eidx in xrange(params.epochs):
                n_samples = 0

                # Get new shuffled index for the training set.
                kf = self.get_minibatches_idx(len(train_data), params.batchsize, shuffle=True)
                uidx = 0
                for _, train_index in kf:

                    uidx += 1
                    batch = [train_data[t] for t in train_index]

                    for i in batch:
                        i[0].populate_embeddings(words)
                        i[1].populate_embeddings(words)

                    (scores,g1x,g1mask,g2x,g2mask) = self.getData(batch)
                    cost = self.train_function(scores, g1x, g2x, g1mask, g2mask)

                    if np.isnan(cost) or np.isinf(cost):
                        print 'NaN detected'

                    #print 'Epoch ', (eidx+1), 'Update ', (uidx+1), 'Cost ', cost

                                        #undo batch to save RAM
                    for i in batch:
                        i[0].representation = None
                        i[1].representation = None
                        i[0].unpopulate_embeddings()
                        i[1].unpopulate_embeddings()

                dp,ds = evaluate(self,words,dev,"dev:")
                tp,ts = evaluate(self,words,test,"test:")
                rp,rs = evaluate(self,words,train,"test:")
                print "evaluation: ",dp,ds,tp,ts,rp,rs

                print 'Epoch ', (eidx+1), 'Cost ', cost

            print 'Seen %d samples' % n_samples


        except KeyboardInterrupt:
            print "Training interupted"

        end_time = time.time()
        print "total time:", (end_time - start_time)