from scipy.spatial.distance import cosine
from scipy.stats import spearmanr
from scipy.stats import pearsonr
from utils import lookupIDX
import pdb
import numpy as np

def getSeqs(p1,p2,model,words):
    p1 = p1.split()
    p2 = p2.split()
    X1 = []
    X2 = []
    for i in p1:
        X1.append(lookupIDX(words,i))
    for i in p2:
        X2.append(lookupIDX(words,i))
    return X1, X2

def getScore(p1,p2,model,words):
    p1 = p1.split()
    p2 = p2.split()
    X1 = []
    X2 = []
    for i in p1:
        X1.append(lookupIDX(words,i))
    for i in p2:
        X2.append(lookupIDX(words,i))
    seqs = [X1]
    x1,m1 = model.prepare_data(seqs)
    seqs = [X2]
    x2,m2 = model.prepare_data(seqs)
    scores = model.scoring_function(x1,x2,m1,m2)
    scores = np.squeeze(scores)
    return float(scores)

def acc(preds,scores):
    correct = 0
    total = 0
    for n,i in enumerate(scores):
        p = -1
        i=i.strip()
        if i == "CONTRADICTION":
            p = 0
        if i == "NEUTRAL":
            p = 1
        if i == "ENTAILMENT":
            p = 2
        #print p, preds[n], i
        #print p, preds[n]
        if p == preds[n]:
            correct +=1
        total += 1
    return float(correct)/total

def getAcc(model,words,f):
    f = open(f,'r')
    lines = f.readlines()
    preds = []
    golds = []
    seq1 = []
    seq2 = []
    ct = 0
    for i in lines:
        i = i.split("\t")
        p1 = i[0]; p2 = i[1]; score = i[2]
        X1, X2 = getSeqs(p1,p2,model,words)
        seq1.append(X1)
        seq2.append(X2)
        ct += 1
        if ct % 100 == 0:
            x1,m1 = model.prepare_data(seq1)
            x2,m2 = model.prepare_data(seq2)
            scores = model.scoring_function(x1,x2,m1,m2)
            #pdb.set_trace()
            scores = np.squeeze(scores)
            preds.extend(scores.tolist())
            seq1 = []
            seq2 = []
        golds.append(score)
    if len(seq1) > 0:
        x1,m1 = model.prepare_data(seq1)
        x2,m2 = model.prepare_data(seq2)
        scores = np.squeeze(scores)
        preds.extend(scores.tolist())
    return acc(preds,golds)

def evaluate(model,words,file,out):
    s = getAcc(model,words,file)
    return s



